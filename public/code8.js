const preperson = JSON.parse(localStorage.getItem('person'));
const input1 = document.getElementsByName('name');
const input2 = document.getElementsByName('secondname');
const input3 = document.getElementsByName('email');
window.onload = function () {
input1[0].value = preperson.name;
input2[0].value = preperson.secondname;
input3[0].value = preperson.email;
console.log(input1[0]);
}

$(document).mouseup(function (e) {

    var popup = $('.js-popup-campaign');
    if (e.target != popup[0] && popup.has(e.target).length === 0) {
        $('.js-overlay-campaign').fadeOut();
        history.pushState({page: 1}, "", "page.html");
    }
});

window.onbeforeunload = function () {
    let person = {
        name: input1[0].value,
        secondname: input2[0].value,
        emain: input3[0].value,
    }
    localStorage.setItem("person", JSON.stringify(person));
    console.log(localStorage.getItem(person));
    console.log(input1[0].value);
    return false;
};

$(window).on('load', function () {
    setTimeout(function () {
        if ($('.js-overlay-campaign').hasClass('disabled')) {
            return false;
        } else {

            $(".js-overlay-campaign").fadeIn();
        }
    }, 5);
});


$(function(){
    $(".ajaxForm").submit(function(e){
        e.preventDefault();
        var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("We received your submission, thank you!");
                }else{
                    alert("An error occured: " + response.message);
                }
            }
        });
    });
});